//
//  TestDefaultReviewHeader.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import XCTest
@testable import Reviewer

class TestDefaultReviewHeader: XCTestCase {
    var reviews = [Review]()

    override func setUp() {
        super.setUp()
        reviews = JSONParser.fetchReviews("articles")!.reviews
    }
    
    func testDetails() {
        let defaultHeaderView = DefaultReviewHeaderView(review: reviews[0])
        
        XCTAssertEqual("The revenant", defaultHeaderView.movieTitle, "these should match")
        XCTAssertEqual("Leo's beautiful endurance test", defaultHeaderView.movieDescription)
        XCTAssertEqual(4, defaultHeaderView.rating)
        XCTAssertEqual("http://www.telegraph.co.uk/content/dam/film/the revenant/leo-xlarge.jpg", defaultHeaderView.imageURL)
        let expectation = expectationWithDescription("fetch image")
        
        defaultHeaderView.fetchMovieImage(defaultHeaderView.imageURL) { (data) in
            XCTAssertNotNil(data)
            
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5) { (error) in
            if let currentError = error {
                print("error \(currentError.localizedDescription)")
            }
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

}
