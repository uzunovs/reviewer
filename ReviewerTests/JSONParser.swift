//
//  JSONParser.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation
@testable import Reviewer

class JSONParser {
    class func fetchReviews(fileName: String) -> Collection? {
        if let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "json")
        {
            if let jsonData = try? NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedAlways)
            {
                if let jsonResult = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as? JSONDictionary
                {
                    return Collection(dictionary: jsonResult!)
                }
            }
        }
        
        return nil
    }
}