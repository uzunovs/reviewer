//
//  TestReviewDetailControllerManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import XCTest
@testable import Reviewer

///
class TestReviewDetailControllerManager: XCTestCase {
    
    var reviews = [Review]()

    override func setUp() {
        super.setUp()
        reviews = JSONParser.fetchReviews("articles")!.reviews
        
    }
    
    func testManager() {
        let manager = ReviewDetailManager(review: reviews[0])
        
        XCTAssertEqual(5, manager.managers.count)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

}
