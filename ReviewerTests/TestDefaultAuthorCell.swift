//
//  TestDefaultAuthorCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import XCTest
@testable import Reviewer

class TestDefaultAuthorCell: XCTestCase {
    
    var reviews = [Review]()

    override func setUp() {
        super.setUp()
        reviews = JSONParser.fetchReviews("articles")!.reviews
    }
    
    func testDefaultAuthorImplementation() {
        let defaultAuthorCell = DefaultAuthorCell(review: reviews[0])
        
        XCTAssertEqual("Robbie Collin", defaultAuthorCell.authorName)
        XCTAssertEqual("http://www.telegraph.co.uk/content/dam/film/team/robbie-collin-byline0-USE-small.jpg", defaultAuthorCell.authorImageURL)
        XCTAssertEqual("@robbiereviews", defaultAuthorCell.authorTwitter)
        
        let expectation = expectationWithDescription("fetch author picture")
        
        defaultAuthorCell.fetchAuthorImage(defaultAuthorCell.authorImageURL) { (data) in
            XCTAssertNotNil(data)
            
            expectation.fulfill()
            
        }
        
        waitForExpectationsWithTimeout(5) { (error) in
            if let currentError = error {
                print("error \(currentError.localizedDescription)")
            }
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

}
