//
//  ReviewsManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation
import UIKit

///The insets for the collection view content
let PreferredEdgeInset: CGFloat = 10

///Manages the cotroller for the reviews
class ReviewsManager: NSObject {
    ///The managers that will be managed by this manager
    lazy private var localManagers = [CollectionViewCellManagerProtocol]()
    
    ///The reviews that the API gives to us
    var reviews = [Review]() {
        willSet {
            //prepare a manager for each review
            for review in newValue {
                localManagers.append(SummaryCellManager(review: review))
            }
        }
        didSet {
            //update the UI
            viewController?.collectionView.reloadData()
        }
    }
    ///The view controller that is managed by this manager
    weak private var viewController: ReviewsViewController?
    
    override init() {
        super.init()
        //as soon as we get alive
        //fetch the reviews
        NetworkManager.fetchReviews({ (collection) in
            if let reviews = collection {
                dispatch_async(dispatch_get_main_queue(), {
                    self.reviews = reviews.reviews
                })
            }
        })
    }
}

extension ReviewsManager: CollectionViewManagerProtocol {
    
    func notifySubmanagersAbout(parameter: Any?) {
        
    }
    //dummy implementation
    func messageFromSubmanager(parameter: Any?) {
        if let incomming = parameter as? String {
            if incomming == ManagerMessage.invalidateLayout {
                viewController?.collectionView.collectionViewLayout.invalidateLayout()
            }
            else if incomming == ManagerMessage.reloadData {
                viewController?.collectionView.reloadData()
            }
        }
    }
    //dummy implementation
    func willAppear() {
        for manager in localManagers {
            manager.willAppear()
        }
    }
    
    var managers: [CollectionViewCellManagerProtocol] {
        return localManagers
    }
    
    func messageFromController(parameter: Any?) -> Any? {
        if let itemNumber = parameter as? Int {
            return reviews[itemNumber]
        }
        
        if let controller = parameter as? ReviewsViewController {
            viewController = controller
        }
        
        return nil
    }
}

