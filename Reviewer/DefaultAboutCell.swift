//
//  DefaultAboutCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Creates a default presentation for a TwoColumnsCell that is setup as a about cell
struct DefaultAboutCell {
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

///Make it compliant with the TwoColumnsProtocol
extension DefaultAboutCell: TwoColumnsProtocol {
    var leftTitle: String {
        return "Synopsis"
    }
    
    var rightTitle: String {
        return "Genre"
    }
    
    var leftText: String {
        return localReview.synopsis
    }
    
    var rightText: [String] {
        return localReview.genre
    }
}