//
//  CollectionViewManagerProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation



//Provides an abstraction of some specific tasks that such a manager has to do
protocol CollectionViewManagerProtocol: TopLevelManagerProtocol, ControllerToManagerCommunication {
    
    var managers: [CollectionViewCellManagerProtocol] { get }
}

extension ManagerMessage {
    ///Ask the manager to invalidate the layout of the collection view
    static let invalidateLayout = "InvalidateLayout"
    ///Ask the manager to reload data for the collection view
    static let reloadData = "ReloadData"
}