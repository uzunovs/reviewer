//
//  NibDefinable.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

protocol NibDefinable {
    var nibName: String { get }
}

extension NibDefinable {
    var nibName: String {
        return String(self.dynamicType)
    }
}