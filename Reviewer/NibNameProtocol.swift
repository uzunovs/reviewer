//
//  NibNameProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Provides interface to define a nib name
protocol NibNameProtocol {
    var nibName: String { get }
}