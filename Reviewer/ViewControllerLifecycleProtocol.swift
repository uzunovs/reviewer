//
//  ViewControllerLifecycleProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Provides a way for the managers/model objects to jack into the lifecycle of the view controller
protocol ViewControllerLifecycleProtocol {
    func willAppear()
    func willDisappear()
    func didAppear()
    func didDisappear()
    
}

//A lot of times there will be nothing for the manager to do with these methods. So we just provide a default implementation that does nothing instead of writing a lot of boilerplate code
extension ViewControllerLifecycleProtocol {
    func willAppear() {
        
    }
    
    func willDisappear() {
        
    }
    
    func didAppear() {
        
    }
    
    func didDisappear() {
        
    }
}