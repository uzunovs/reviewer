//
//  CollectionViewCellManagerProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

protocol CollectionViewCellManagerProtocol: CellReuseIdentifierProtocol, NibNameProtocol, ViewControllerLifecycleProtocol {
    /**
     The manager takes all neccessary steps to properly instantiate the cell
     
     - Parameter collectionView: The collection view that requests the cell
     - Parameter atIndexPath: The indexPath of the cell
     
     - returns: A collection view cell
     */
    mutating func setupCell(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    
}