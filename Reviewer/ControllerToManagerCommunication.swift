//
//  ControllerToManagerCommunication.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

protocol ControllerToManagerCommunication {
    func messageFromController(parameter: Any?) -> Any?
}

extension ControllerToManagerCommunication {
    func messageFromController(parameter: Any?) -> Any? {
        return nil
    }
}