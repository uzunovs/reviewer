//
//  ReviewsViewController.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Provides the UI for the reviews
class ReviewsViewController: UIViewController {
    ///Who is managing this controller
    lazy var manager: CollectionViewManagerProtocol = {
       let manager = ReviewsManager()
        return manager
    }()
    ///What review was selected by the user
    var selectedReview: Review?
    ///Expose the collection view as read only
    var collectionView: UICollectionView {
        return reviewsCollectionView
    }
    ///The collection view that shows the reviews
    @IBOutlet private weak var reviewsCollectionView: UICollectionView!
    
    ///The segue to the review details
    private let ShowDetailSegueIdentifier = "ShowDetail"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set our background image as the background
        reviewsCollectionView.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
        //perform setup tasks
        manager.messageFromController(self)
        
        reviewsCollectionView.dataSource = self
        reviewsCollectionView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        manager.willAppear()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            if let currentReview = selectedReview {
                if let controller = segue.destinationViewController as? ReviewDetailsViewController {
                    //give the review to the review details controller
                    controller.review = currentReview
                }
            }
        }
    }

}

extension ReviewsViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return manager.managers.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let managers = manager.managers
        var cellManager = managers[indexPath.item]
        let cell = cellManager.setupCell(collectionView, atIndexPath: indexPath)
        
        return cell
    }
}

extension ReviewsViewController: UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.item < manager.managers.count {
            if let review = manager.messageFromController(indexPath.item) as? Review {
                selectedReview = review
            }
            performSegueWithIdentifier(ShowDetailSegueIdentifier, sender: self)
        }
    }
}

extension ReviewsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - (2 * PreferredEdgeInset), height: 150)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: PreferredEdgeInset, left: PreferredEdgeInset, bottom: PreferredEdgeInset, right: PreferredEdgeInset)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeZero
    }
}
