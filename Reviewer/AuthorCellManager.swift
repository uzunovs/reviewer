//
//  AuthorCellManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Manages an AuthorCell
class AuthorCellManager {
    ///The URL to twitter
    private let twitterURL = "twitter://user?screen_name="
    ///The cell the manager manages
    private var cell = AuthorCollectionViewCell()
    ///The local storage for the review
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

extension AuthorCellManager: CollectionViewCellManagerProtocol {
    var cellReuseIdentifier: String {
        return "Author"
    }
    
    var nibName: String {
        return "AuthorCollectionViewCell"
    }
    
    func setupCell(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerNib(UINib(nibName: nibName, bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: cellReuseIdentifier)
        
        cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! AuthorCollectionViewCell
        cell.setupCell(DefaultAuthorCell(review: localReview))
        
        cell.openTwitter = {
            if let twitterURL = NSURL(string: "\(self.twitterURL)\(self.localReview.author.twitter)") {
                if UIApplication.sharedApplication().canOpenURL(twitterURL) {
                    UIApplication.sharedApplication().openURL(twitterURL)
                }
            }
            
        }
        
        return cell
    }
}