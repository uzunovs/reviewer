//
//  DefaultReviewHeaderView.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Creates an default implementation for the header view
struct DefaultReviewHeaderView {
    
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

extension DefaultReviewHeaderView: ReviewHeaderViewProtocol {
    //tell the protocol where to look for the values
    var movieTitle: String {
        return localReview.headline
    }
    
    var movieDescription: String {
        return localReview.description
    }
    
    var rating: Int {
        return localReview.ratings
    }
    
    var imageURL: String {
        return localReview.pictureURL
    }
    
}