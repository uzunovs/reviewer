//
//  SummaryCellManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Manages a summary cell
struct SummaryCellManager {
    ///The cell this manager manages
    private var cell = ReviewSummaryCollectionViewCell()
    ///The model object
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

extension SummaryCellManager: CollectionViewCellManagerProtocol {
    var cellReuseIdentifier: String {
        return "Summary"
    }
    
    var nibName: String {
        return "ReviewSummaryCollectionViewCell"
    }
    
    mutating func setupCell(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //this manager takes care of registering the nib for the collection view
        collectionView.registerNib(UINib(nibName: nibName, bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: cellReuseIdentifier)
        //deque the cell
        cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! ReviewSummaryCollectionViewCell
        //set it up for use
        cell.summaryView.setup(DefaultReviewHeaderView(review: localReview))
        
        return cell
    }
}

