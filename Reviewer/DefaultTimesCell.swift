//
//  DefaultTimesCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Creates a default presentation for the TimesCell
struct DefaultTimesCell {
    ///The local storage for the review
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

//Make it compliant with the TimesCellProtocol
extension DefaultTimesCell: TimesCellProtocol {
    var duration: String {
        return localReview.duration
    }
    
    var releasedDate: String {
        return localReview.releaseDate
    }
    
    var publishedDate: String {
        return localReview.datePublished
    }
}