//
//  ReviewDetailsViewController.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Provides the UI for the review details
class ReviewDetailsViewController: UIViewController {
    ///Expose the collection view as read only
    var collectionView: UICollectionView {
        return reviewDetailCollectionView
    }
    //What was the review that the user selected
    var review: Review?
    ///The manager that manages this controller
    lazy var manager: ReviewDetailManager = {
        let manager = ReviewDetailManager(review: self.review!)
        return manager
    }()
    ///The collection view that contains the review details
    @IBOutlet private weak var reviewDetailCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set our background image as the background
        reviewDetailCollectionView.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
        
        reviewDetailCollectionView.dataSource = self
        reviewDetailCollectionView.delegate = self
    }
}

extension ReviewDetailsViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return manager.managers.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let managers = manager.managers
        
        var cellManager = managers[indexPath.item]
        
        let cell = cellManager.setupCell(collectionView, atIndexPath: indexPath)
        
        return cell
    }
}

extension ReviewDetailsViewController: UICollectionViewDelegate {
    
}

extension ReviewDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - (2 * PreferredEdgeInset), height: 150)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: PreferredEdgeInset, left: PreferredEdgeInset, bottom: PreferredEdgeInset, right: PreferredEdgeInset)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeZero
    }
}
