//
//  DefaultAuthorCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Creates a default presentation for the AuthorCell
struct DefaultAuthorCell {
    ///The local storage for the review
    private let localReview: Review
    ///The image data
    private var imageData: NSData?
    
    init(review: Review) {
        localReview = review
    }
}

extension DefaultAuthorCell: AuthorCellProtocol {
    var authorName: String {
        return localReview.author.name
    }
    
    var authorTwitter: String {
        return localReview.author.twitter
    }
    
    var authorImageURL: String {
        return localReview.author.headshot
    }
    
    var authorImage: NSData? {
        get {
            return imageData
        }
        set {
            imageData = newValue
        }
    }
}