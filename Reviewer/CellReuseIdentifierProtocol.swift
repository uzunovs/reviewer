//
//  CellReuseIdentifierProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Provides interface to define a cell reuse identifier
protocol CellReuseIdentifierProtocol {
    var cellReuseIdentifier: String { get }
}