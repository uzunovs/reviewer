//
//  TimesCellProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///The protocol that describes a TimeCell
protocol TimesCellProtocol {
    ///The duration of the movie
    var duration: String { get }
    ///The date it was published
    var publishedDate: String { get }
    ///The date it was released
    var releasedDate: String { get }
}