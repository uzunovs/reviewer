//
//  ReviewSummaryCollectionViewCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///A cell that is used as a preview of a review
class ReviewSummaryCollectionViewCell: UICollectionViewCell {
    ///The view that contains the review
    @IBOutlet private weak var review: ReviewHeaderView!
    ///Exposes the review view as read only
    var summaryView: ReviewHeaderView {
        return review
    }
}
