//
//  AuthorCollectionViewCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Defines the UI for the AuthorCell
class AuthorCollectionViewCell: UICollectionViewCell {
    ///The author image
    @IBOutlet private weak var authorImageView: UIImageView!
    ///The author name
    @IBOutlet private weak var nameLabel: UILabel!
    ///The author Twitter account
    @IBOutlet private weak var twitterLinkLabel: UILabel!
    ///Callback for when the user taps on the twitter link
    var openTwitter: OpenURL?
    /**
     Translates the AuthorCellProtocol to the UI
     
     - Parameter master: The object that holds the customisation data
     */
    func setupCell(master: AuthorCellProtocol) {
        nameLabel.text = master.authorName
        twitterLinkLabel.text = master.authorTwitter
        twitterLinkLabel.addGestureRecognizer(tapGesture)
        
        master.fetchAuthorImage(master.authorImageURL) { (data) in
            guard nil != data else {
                return
            }
            
            if let validData = data {
                //if we have the data
                //fetch the main thread and give the imageView the image
                dispatch_async(dispatch_get_main_queue(), {
                    self.authorImageView.image = UIImage(data: validData)
                })
            }
        }
    }
    ///The gesture that will be recognised when the user taps on the Twitter link
    lazy private var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.twitterTagTapped(_:)))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        return gesture
    }()
    
    /**
     Calls the gesture callback to notify the interested party
     
     - Parameter sender: The gesture that was recognised
     */
    @objc private func twitterTagTapped(sender: UITapGestureRecognizer) {
        openTwitter?()
    }
}
