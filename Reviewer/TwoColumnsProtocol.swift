//
//  TwoColumnsProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///The protocol that describes the TwoColumnCell
protocol TwoColumnsProtocol {
    ///The left-hand side title
    var leftTitle: String { get }
    ///The right-hand side title
    var rightTitle: String { get }
    ///The left-hand side text
    var leftText: String { get }
    ///The right-hand side text
    var rightText: [String] { get }
}