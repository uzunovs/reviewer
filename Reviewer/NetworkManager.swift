//
//  NetworkManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Endpoints of the API
struct Endpoint {
    ///The reviews
    static let reviews = "http://s.telegraph.co.uk/tmgmobilepub/articles.json"
}

/**
 Allows to fetch data from the network
 */
class NetworkManager {
    
    /**
     Fetches the reviews
     
     - Parameter: completion: A completion handler for the network request
     */
    class func fetchReviews(completion: (Collection?) -> Void) {
        //prep the URL
        let url = NSURL(string: Endpoint.reviews)!
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            //if we don't have data
            //there is not much we can do
            guard nil != data else {
                return
            }
            
            do {
                //try to convert the data
                let fetchedReviews = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? JSONDictionary
                if let reviews = fetchedReviews {
                    //if the data is OK and we have a json dict
                    //give the handler the constructed object
                    completion(Collection(dictionary: reviews))
                }
                else {
                    //something went wrong
                    //indicate we don't have valid response
                    completion(nil)
                }
            }
            catch {
                //there was some sort of an issue with the returned data
                //we were not able to create a mapped object
                completion(nil)
            }
        }
        
        task.resume()
    }
    
    /**
     Fetches a image from the URL that is provided
     
     - Parameter: urlString: The URL string where the image resides
     - Parameter: completion: Completion handler for the image data
     */
    class func fetchImage(urlString: String, completion: (NSData?) -> Void) {
        let escapedString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        let url = NSURL(string: escapedString)!
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            guard nil != data else {
                completion(nil)
                return
            }
            
            completion(data)
        }
        
        task.resume()
    }
}