//
//  Review.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///The type of dictionary the API returns
typealias JSONDictionary = [String: AnyObject]

///The type of array the API returns
typealias JSONArray = [AnyObject]

///Maps the data from the API response to a structure in the app
struct Review {
    //fields in the response
    let id: Int
    let websiteURL: String
    let headline: String
    let description: String
    let articleBody: String
    let ratings: Int
    let pictureURL: String
    let videoURL: String
    let actors: [String]
    let director: String
    let genre: [String]
    let synopsis: String
    let releaseDate: String
    let duration: String
    let datePublished: String
    let author: Author
    
    init(dictionary: JSONDictionary) {
        id = dictionary[Key.id] as? Int ?? -1
        websiteURL = dictionary[Key.websiteURL] as? String ?? ""
        headline = dictionary[Key.headline] as? String ?? ""
        description = dictionary[Key.description] as? String ?? ""
        articleBody = dictionary[Key.articleBody] as? String ?? ""
        ratings = dictionary[Key.ratings] as? Int ?? -1
        pictureURL = dictionary[Key.pictureURL] as? String ?? ""
        videoURL = dictionary[Key.videoURL] as? String ?? ""
        actors = dictionary[Key.actors] as? [String] ?? []
        director = dictionary[Key.director] as? String ?? ""
        genre = dictionary[Key.genre] as? [String] ?? []
        synopsis = dictionary[Key.synopsis] as? String   ?? ""
        releaseDate = dictionary[Key.releaseDate] as? String ?? ""
        duration = dictionary[Key.duration] as? String ?? ""
        datePublished = dictionary[Key.datePublished] as? String ?? ""
        author = Author(dictionary: dictionary[Key.author] as? JSONDictionary ?? JSONDictionary())
    }
    
    struct Author {
        let name: String
        let headshot: String
        let twitter: String
        
        init(dictionary: JSONDictionary) {
            name = dictionary[Key.Author.name] as? String ?? ""
            headshot = dictionary[Key.Author.headshot] as? String ?? ""
            twitter = dictionary[Key.Author.twitter] as? String ?? ""
        }
    }
    
    private struct Key {
        static let id = "id"
        static let websiteURL = "website-url"
        static let headline = "headline"
        static let description = "description"
        static let articleBody = "article-body"
        static let ratings = "ratings"
        static let pictureURL = "picture-url"
        static let videoURL = "video-url"
        static let actors = "actors"
        static let director = "director"
        static let genre = "genre"
        static let synopsis = "synopsis"
        static let releaseDate = "release-date"
        static let duration = "duration"
        static let datePublished = "published-date"
        static let author = "author"
        struct Author {
            static let name = "name"
            static let headshot = "headshot"
            static let twitter = "twitter"
        }
    }
}