//
//  UIColorExtension.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit



extension UIColor {
    
    convenience init(customColor: CustomColor) {
        let rgbaValue = customColor.rawValue
        let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
        let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
        let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
}