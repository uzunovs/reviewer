//
//  DetailsHeaderManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///The manager of the detail header cell
class DetailsHeaderManager {
    ///The cell this manager manages
    private var cell = DetailsHeaderCollectionViewCell()
    ///The review this cell presents
    private let localReveiw: Review
    
    init(review: Review) {
        localReveiw = review
    }
}

extension DetailsHeaderManager: CollectionViewCellManagerProtocol {
    var cellReuseIdentifier: String {
        return "HeaderCell"
    }
    
    var nibName: String {
        return "DetailsHeaderCollectionViewCell"
    }
    
    func setupCell(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerNib(UINib(nibName: nibName, bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: cellReuseIdentifier)
        cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! DetailsHeaderCollectionViewCell
        cell.review.setup(DefaultReviewHeaderView(review: localReveiw))
        //this is where the cell manager receives the callback from the info button on the cell
        cell.openURL = {
            //if we can get a good URL
            //open it in Safari
            if let validURL = NSURL(string: self.localReveiw.websiteURL) {
                if UIApplication.sharedApplication().canOpenURL(validURL) {
                    UIApplication.sharedApplication().openURL(validURL)
                }
            }
        }
        
        return cell
    }
}