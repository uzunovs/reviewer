//
//  TwoColumnsCollectionViewCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Defines the UI for a cell with two columns
class TwoColumnsCollectionViewCell: UICollectionViewCell {
    ///The left-hand title
    @IBOutlet private weak var leftTitleLabel: UILabel!
    ///The right-hand title
    @IBOutlet private weak var rightTitleLabel: UILabel!
    ///The left-hand text
    @IBOutlet private weak var leftTextLabel: UILabel!
    ///The right-hand text
    @IBOutlet private weak var rightLabelText: UILabel!
    
    /**
     Translates the TwoColumnsProtocol to the UI
     
     - Parameter master: The object that holds the customisation data
     */
    func setupCell(master: TwoColumnsProtocol) {
        leftTitleLabel.text = master.leftTitle
        rightTitleLabel.text = master.rightTitle
        leftTextLabel.text = master.leftText
        //prepare a local string to hold all the strings in the array
        var names = ""
        //iterate over the array
        for (index, name) in master.rightText.enumerate() {
            if index == 0 {
                //if this is the firs item
                //just add it
                names += name
            }
            else {
                //for all subsequent items
                //add a new line as well
                names += "\n\(name)"
            }
        }
        
        rightLabelText.text = names
    }
}
