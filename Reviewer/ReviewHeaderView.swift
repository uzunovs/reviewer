//
//  ReviewHeaderView.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Creates a view that acts as a header/summary for a movie
class ReviewHeaderView: UIView, NibDefinable {
    ///The image for the movie
    var image: UIImage? {
        willSet {
            if let validImage = newValue {
                //if we have a valid image
                //give it to the image view
                movieImageView.image = validImage
            }
        }
    }
    
    ///The main view
    @IBOutlet private var view: UIView!
    ///Image view for the movie image
    @IBOutlet private weak var movieImageView: UIImageView!
    ///The label for the title of the movie
    @IBOutlet private weak var titleLabel: UILabel!
    ///The label for the short description of the movie
    @IBOutlet private weak var descriptionLabel: UILabel!
    ///The rating view
    @IBOutlet private weak var starRating: FloatRatingView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupXib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupXib()
    }
    
    /**
     Gives data and UI to the view
     
     - Parameter master: The master object that contains the data necessary for the view
     */
    func setup(master: ReviewHeaderViewProtocol) {
        //UI section
        titleLabel.font = UIFont(name: master.headerLabelFont, size: CGFloat( master.headerLabelFontSize))
        titleLabel.textColor = UIColor(customColor: master.headerLabelFontColor)
        
        descriptionLabel.font = UIFont(name: master.descriptionLabelFont, size: CGFloat( master.descriptionLabelFontSize))
        descriptionLabel.textColor = UIColor(customColor: master.descriptionLabelFontColor)
        //data
        titleLabel.text = master.movieTitle
        descriptionLabel.text = master.movieDescription
        starRating.rating = Float(master.rating)
        
        master.fetchMovieImage(master.imageURL) { (data) in
            guard nil != data else {
                return
            }
            
            if let validData = data {
                dispatch_async(dispatch_get_main_queue(), { 
                    self.movieImageView.image = UIImage(data: validData)
                })
            }
        }
    }
    
    
    /**
     Prepares the nib for use
     */
    func setupXib() {
        view = loadViewFromNib()
        view.frame = bounds
        autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(view)
    }
    
    /**
     Loads a view from the nib
     
     - returns: The view that is contained in the view
     */
    private func loadViewFromNib() -> UIView {
        let bundle = NSBundle.mainBundle()
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil).first as! UIView
        
        return view
    }
}
