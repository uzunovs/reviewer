//
//  Collection.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///This is where we keep all the reviews that the API returns
struct Collection {
    ///The reviews
    let reviews: [Review]
    
    init(dictionary: JSONDictionary) {
        var collection = [Review]()
        let array = dictionary[Key.collection] as? JSONArray ?? JSONArray()
        for item in array {
            if let review = item as? JSONDictionary {
                let review = Review(dictionary: review)
                collection.append(review)
            }
        }
        
        reviews = collection
    }
    
    private struct Key {
        static let collection = "collection"
    }
}