//
//  HomeViewController.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///This is the first scene the users are going to see when they launch the app
class HomeViewController: UIViewController {
    ///Contains message to the user in the cases where we don't have network
    @IBOutlet private weak var networkWarningLabel: UILabel!
    ///The CA button
    @IBOutlet private weak var showReviewsButton: UIButton!
    ///Identify the segue
    private let reviewsSegueIdentifier = "ShowReviews"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set our background image as the background
        view.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        checkConnectivity()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Checks if we have connection and adjust the UI accordingly
     */
    private func checkConnectivity() {
        if connectedToNetwork() {
            //we are connected to the network
            //hide the message
            networkWarningLabel.hidden = true
            //enable the button
            showReviewsButton.enabled = true
        }
        else {
            //we are not connected to the network
            //show the message
            networkWarningLabel.hidden = false
            //deactivate the button
            showReviewsButton.enabled = false
        }
    }
 

}
