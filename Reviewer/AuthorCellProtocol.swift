//
//  AuthorCellProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Defines the UI for the AuthorCell
protocol AuthorCellProtocol {
    ///The author name
    var authorName: String { get }
    ///The author twitter name
    var authorTwitter: String { get }
    ///The author image as data
    var authorImage: NSData? { get set }
    ///The author image URL
    var authorImageURL: String { get }
    /**
     Fetches the image of the author from the URL provided
     
     - Parameter imageURL: The URL for the image
     - Parameter completion: A callback when we get the data
     */
    func fetchAuthorImage(imageURL: String, completion: NSData? -> Void)
}

extension AuthorCellProtocol {
    func fetchAuthorImage(imageURL: String, completion: NSData? -> Void) {
        NetworkManager.fetchImage(imageURL) { (data) in
            guard nil != data else {
                completion(nil)
                return
            }
            
            completion(data!)
        }
    }
}