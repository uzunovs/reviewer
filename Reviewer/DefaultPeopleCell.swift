//
//  DefaultPeopleCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Define a default presentation for the people cell
struct DefaultPeopleCell {
    ///Local storage for the review
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

///Make it compliant with the TwoColumnsProtocol
extension DefaultPeopleCell: TwoColumnsProtocol {
    var leftTitle: String {
        return "Director"
    }
    
    var rightTitle: String {
        return "Cast"
    }
    
    var leftText: String {
        return localReview.director
    }
    
    var rightText: [String] {
        return localReview.actors
    }
}