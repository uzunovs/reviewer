//
//  DetailsHeaderCollectionViewCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Callback action when the user taps on the info button
typealias OpenURL = () -> ()

///Defines a cell for the details
class DetailsHeaderCollectionViewCell: UICollectionViewCell {
    ///The pointer to the review header view
    @IBOutlet private weak var reviewView: ReviewHeaderView!
    ///Expose a read only property
    var review: ReviewHeaderView {
        return reviewView
    }
    
    var openURL: OpenURL?
    
    /**
     Notifies to try and open the website link
     
     - Parameter sender: The button that invoked the method
     */
    @IBAction private func openWebsite(sender: UIButton) {
        //pass the action to the receiver
        openURL?()
    }
}
