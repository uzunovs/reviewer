//
//  TimesCollectionViewCell.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Defines the UI for a TimesCollectionViewCell
class TimesCollectionViewCell: UICollectionViewCell {
    ///The published date label
    @IBOutlet private weak var publishedDateLabel: UILabel!
    ///The duration of the movie label
    @IBOutlet private weak var durationLabel: UILabel!
    ///The released date label
    @IBOutlet private weak var releasedDateLabel: UILabel!
    
    /**
     Translates the TimesCellProtocol to the UI
     
     - Parameter master: The object that holds the customisation data
     */
    func setupCell(master: TimesCellProtocol) {
        publishedDateLabel.text = master.publishedDate
        durationLabel.text = master.duration
        releasedDateLabel.text = master.releasedDate
    }
}
