//
//  TimesCellManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Manages a TimesCell
class TimesCellManager {
    ///The cell this manager manages
    private var cell = TimesCollectionViewCell()
    ///The local storage for the review
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

extension TimesCellManager: CollectionViewCellManagerProtocol {
    var cellReuseIdentifier: String {
        return "Times"
    }
    
    var nibName: String {
        return "TimesCollectionViewCell"
    }
    
    func setupCell(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerNib(UINib(nibName: nibName, bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: cellReuseIdentifier)
        cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! TimesCollectionViewCell
        cell.setupCell(DefaultTimesCell(review: localReview))
        
        return cell
    }
}