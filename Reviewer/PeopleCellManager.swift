//
//  PeopleCellManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Manages a TwoColumnsCollectionViewCell that has been set up as a PeopleCell
class PeopleCellManager {
    ///The cell this manager manages
    private var cell = TwoColumnsCollectionViewCell()
    ///Local storage for the review
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

extension PeopleCellManager: CollectionViewCellManagerProtocol {
    var cellReuseIdentifier: String {
        return "TwoColumns"
    }
    
    var nibName: String {
        return "TwoColumnsCollectionViewCell"
    }
    
    func setupCell(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerNib(UINib(nibName: nibName, bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: cellReuseIdentifier)
        cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! TwoColumnsCollectionViewCell
        
        cell.setupCell(DefaultPeopleCell(review: localReview))
        
        return cell
        
    }
}