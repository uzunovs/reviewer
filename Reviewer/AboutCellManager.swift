//
//  AboutCellManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

///Manages a TwoColumnsCell as an AboutCell
class AboutCellManager {
    ///The cell this manager manages
    private var cell = TwoColumnsCollectionViewCell()
    ///The local storage for the review
    private let localReview: Review
    
    init(review: Review) {
        localReview = review
    }
}

extension AboutCellManager: CollectionViewCellManagerProtocol {
    var cellReuseIdentifier: String {
        return "TwoColumns"
    }
    
    var nibName: String {
        return "TwoColumnsCollectionViewCell"
    }
    
    func setupCell(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        collectionView.registerNib(UINib(nibName: nibName, bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: cellReuseIdentifier)
        cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! TwoColumnsCollectionViewCell
        
        cell.setupCell(DefaultAboutCell(review: localReview))
        
        return cell
        
    }
}
