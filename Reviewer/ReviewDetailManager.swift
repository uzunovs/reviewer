//
//  ReviewDetailManager.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 04/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import UIKit

class ReviewDetailManager: NSObject {
    ///The managers that this manager is going to manage
    lazy private var localManagers = [CollectionViewCellManagerProtocol]()
    ///The review that was selected by the user
    private let localReview: Review
    
    var managers: [CollectionViewCellManagerProtocol] {
        return localManagers
    }
    
    init(review: Review) {
        localReview = review
        super.init()
        setupManagers()
    }
    
    /**
     Instantiates the managers for the different cells in the collection view. The sequence in the array matters
     */
    private func setupManagers() {
        let header = DetailsHeaderManager(review: localReview)
        let people = PeopleCellManager(review: localReview)
        let times = TimesCellManager(review: localReview)
        let about = AboutCellManager(review: localReview)
        let author = AuthorCellManager(review: localReview)
        localManagers = [header, people, times, about, author]
    }
}

extension ReviewDetailManager: CollectionViewManagerProtocol {
    
    
    func notifySubmanagersAbout(parameter: Any?) {
        
    }
    
    func messageFromSubmanager(parameter: Any?) {
        
    }

    
}


