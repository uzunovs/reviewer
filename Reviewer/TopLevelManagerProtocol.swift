//
//  TopLevelManagerProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

protocol TopLevelManagerProtocol: ViewControllerLifecycleProtocol, InterManagerCommunicationProtocol {
}