//
//  ReviewHeaderViewProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 02/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

enum CustomColor : UInt32 {
    case Black = 0x000000
}

///Creates an abstraction of the header view
protocol ReviewHeaderViewProtocol {
    ///The movie title
    var movieTitle: String { get }
    ///The movie short description
    var movieDescription: String { get }
    ///The rating of the movie
    var rating: Int { get }
    ///The header font
    var headerLabelFont: String { get }
    ///The header font color
    var headerLabelFontColor: CustomColor { get }
    ///The header fort size
    var headerLabelFontSize: Double { get }
    ///The description font
    var descriptionLabelFont: String { get }
    ///The description font color
    var descriptionLabelFontColor: CustomColor { get }
    ///The description font size
    var descriptionLabelFontSize: Double { get }
    ///The URL to the image
    var imageURL: String { get }
    ///Fetches the data associated with the image
    func fetchMovieImage(imageURL: String, completion: NSData? -> Void)
}

//Create a default implementation for the UI
extension ReviewHeaderViewProtocol {
    var headerLabelFont: String {
        return "AvenirNext-Bold"
    }
    
    var headerLabelFontColor: CustomColor {
        return .Black
    }
    
    var headerLabelFontSize: Double {
        return 20.0
    }
    
    var descriptionLabelFont: String {
        return "AmericanTypewriter"
    }
    
    var descriptionLabelFontColor: CustomColor {
        return .Black
    }
    
    var descriptionLabelFontSize: Double {
        return 15.0
    }
    
    func fetchMovieImage(imageURL: String, completion: NSData? -> Void) {
        NetworkManager.fetchImage(imageURL) { (data) in
            completion(data)
        }
    }
}