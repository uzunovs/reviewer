//
//  InterManagerCommunicationProtocol.swift
//  Reviewer
//
//  Created by Hristo Uzunov on 03/08/2016.
//  Copyright © 2016 HristoUzunov. All rights reserved.
//

import Foundation

///Provides a standard set of messages that the top level managers understand
struct ManagerMessage {
    
}

///Provides a standard way for the managers to pass messages
protocol InterManagerCommunicationProtocol {
    /**
     Allows the communication between manager/submanagers and submanagers
     
     - Parameter: parameter: Some object that is needed
     */
    func notifySubmanagersAbout(parameter: Any?)
    
    /**
     Allows the submanager to notify the manager about something
     
     - Parameter: parameter: The message that is passed along
     */
    func messageFromSubmanager(parameter: Any?)
}